myAVR MK2 example: LCD
======================

example program for the Dot Matrix LCD

Copyright (c) 2016 Martin Singer <martin.singer@web.de>


Requires
--------
* avr-gcc
* avrdude


Configuration
-------------

* myAVR MK2 board
* myAVR LCD V2.5 Add-On board
* mySmartUSB MK2 programmer

<http://www.myavr.com>


About
-----

* Dot Matrix LCD:
  2 lines, 16 columns, a 5x7 pixel
* Controller:
  AVR ATmega8L, 3.6864 MHz
* Function:
  - Writes example text on LCD
  - Polls switches which turn LEDs on and off
* Circuit:
  - PortC.0 := Switch 1
  - PortC.1 := Switch 2

  - PortC.3 := LED 1 (green)
  - PortC.4 := LED 2 (yellow)
  - PortC.5 := LED 3 (red)

  - PortB.0 := LCD R/W (read/write), JP R/W
  - PortB.1 := LCD PWM (backlight),  JP PWM
  - PortD.2 := LCD RS  (reset)
  - PortD.3 := LCD E   (enable)
  - PortD.4 := LCD DB4 (databit 4 of LCD)
  - PortD.5 := LCD DB5 (databit 5 of LCD)
  - PortD.6 := LCD DB6 (databit 6 of LCD)
  - PortD.7 := LCD DB7 (databit 7 of LCD)

