/**********************************************************************
 myAVR MK2 example: LCD - example program for the Dot Matrix LCD
 Copyright (c) 2016 Martin Singer <martin.singer@web.de>
 **********************************************************************/

#include "def.h"

// Prototypes
void lcd_init();
void lcd_command(uint8_t);
void lcd_data(uint8_t);
void lcd_text(char *);
void lcd_pos(uint8_t, uint8_t);
